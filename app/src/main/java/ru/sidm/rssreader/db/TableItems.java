package ru.sidm.rssreader.db;

import android.provider.BaseColumns;

/**
 * Created by sidm on 26.02.2016.
 */
public class TableItems implements BaseColumns {

    public static final String TABLE_NAME = "items";
    public static final String COL_ITEM_ID = "item_id"; // item id from internet
    public static final String COL_FEED_ID = "feed_id";
    public static final String COL_TITLE = "title";
    public static final String COL_DATE = "date";
    public static final String COL_LINK = "link";
    public static final String COL_DESCRIPTION = "description";

    public static final String CREATE_TABLE_SCRIPT =
            String.format("create table if not exists %1$s ("
                    + " %2$s text primary key, "
                    + " %3$s integer, "
                    + " %4$s text, "
                    + " %5$s text, "
                    + " %6$s text, "
                    + " %7$s text) "
                    ,TABLE_NAME
                    ,COL_ITEM_ID    // item id from internet
                    ,COL_FEED_ID
                    ,COL_TITLE
                    ,COL_DATE
                    ,COL_LINK
                    ,COL_DESCRIPTION);
}
