package ru.sidm.rssreader.db;

import android.provider.BaseColumns;

/**
 * Created by sidm on 26.02.2016.
 */
public class TableFeeds implements BaseColumns{

    public static final String TABLE_NAME = "feeds";
    public static final String COL_TITLE = "title";
    public static final String COL_LINK = "link";
    public static final String COL_LAST_UPDATE = "lastupdate";
    public static final String COL_BUILTIN = "builtin";

    public static final String CREATE_TABLE_SCRIPT =
            String.format("create table if not exists %1$s ("
                    + " %2$s integer primary key autoincrement, "
                    + " %3$s text, "
                    + " %4$s text, "
                    + " %5$s integer, "
                    + " %6$s integer)" //boolean
                    ,TABLE_NAME
                    ,_ID
                    ,COL_TITLE
                    ,COL_LINK
                    ,COL_LAST_UPDATE
                    ,COL_BUILTIN);
}
