package ru.sidm.rssreader.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.Observable;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBar;

import java.util.ArrayList;
import java.util.List;

import ru.sidm.rssreader.objects.Feed;
import ru.sidm.rssreader.objects.Item;

/**
 * Created by sidm on 26.02.2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private final static String DATABASE_NAME = "rssreader.db";
    private final static int DATABASE_VERSION = 1;
    private static volatile DatabaseHelper instance;
    private SQLiteDatabase database;
    private DatabaseUpdateObservers observers = new DatabaseUpdateObservers();

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        database = getWritableDatabase();
    }

    public static DatabaseHelper getInstance(Context context){
        DatabaseHelper localInstanse = instance;
        if(localInstanse == null){
            synchronized(DatabaseHelper.class){
                localInstanse = instance;
                if(localInstanse == null){
                    localInstanse = instance = new DatabaseHelper(context);
                }

            }
        }

        return localInstanse;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TableFeeds.CREATE_TABLE_SCRIPT);
        db.execSQL(TableItems.CREATE_TABLE_SCRIPT);

        addBuiltInFeeds(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public interface DatabaseUpdateObserver{
        void onDatabaseUpdate();
    }

    public List<Feed> getFeeds(){
        String querry = "select "
                + "  " + TableFeeds._ID
                + ", " + TableFeeds.COL_TITLE
                + ", " + TableFeeds.COL_LINK
                + ", " + TableFeeds.COL_BUILTIN
                + ", " + TableFeeds.COL_LAST_UPDATE
                + " from "
                + "  " + TableFeeds.TABLE_NAME;
        Cursor querryResult = database.rawQuery(querry, null);
        List<Feed> feeds = new ArrayList<>(querryResult.getCount());
        while(querryResult.moveToNext()){
            Feed feed = new Feed(querryResult);
            feeds.add(feed);
        }

        querryResult.close();
        return feeds;
    }

    public List<Item> getItemsByFeedID(int feedID){
        String querry = "select "
                + "  " + TableItems.COL_ITEM_ID
                + ", " + TableItems.COL_FEED_ID
                + ", " + TableItems.COL_TITLE
                + ", " + TableItems.TABLE_NAME + "."+ TableItems.COL_DATE
                + ", " + TableItems.COL_DESCRIPTION
                + ", " + TableItems.COL_LINK
                + " from "
                + "  " + TableItems.TABLE_NAME
                + " where "
                + "  " + TableItems.COL_FEED_ID + " = " + feedID;
        Cursor querryResult = database.rawQuery(querry, null);

        List<Item> items = new ArrayList<>(querryResult.getCount());
        while(querryResult.moveToNext()){
            Item item = new Item(querryResult);
            items.add(item);
        }

        querryResult.close();

        return items;
    }

    public Item getItemByID(String itemID){
        String querry = "select "
                + "  " + TableItems.COL_ITEM_ID
                + ", " + TableItems.COL_FEED_ID
                + ", " + TableItems.COL_TITLE
                + ", " + TableItems.TABLE_NAME + "."+ TableItems.COL_DATE
                + ", " + TableItems.COL_DESCRIPTION
                + ", " + TableItems.COL_LINK
                + " from "
                + "  " + TableItems.TABLE_NAME
                + " where "
                + "  " + TableItems.COL_ITEM_ID + " = \"" + itemID + "\"";
        Cursor querryResult = database.rawQuery(querry, null);

        Item item = null;
        if(querryResult.moveToNext()){
            item = new Item(querryResult);
        }

        querryResult.close();

        return item;
    }


    public void addItemsToDB(List<Item> items){

        ContentValues cv = new ContentValues();
        for(Item item : items){
            cv.put(TableItems.COL_ITEM_ID, item.getItem_id());
            cv.put(TableItems.COL_FEED_ID, item.getFeed_id());
            cv.put(TableItems.COL_DATE, item.getDate().getTime());
            cv.put(TableItems.COL_TITLE, item.getTitle());
            cv.put(TableItems.COL_DESCRIPTION, item.getDescription());
            cv.put(TableItems.COL_LINK, item.getLink());

            database.insertWithOnConflict(TableItems.TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
        }

        observers.notifyObserversDatabaseChanged();
    }

    public void addFeedsToDB(List<Feed> feeds){

        for(Feed feed : feeds){
            addFeedToDB(feed);
        }
    }

    public long addFeedToDB(Feed feed){

        ContentValues cv = new ContentValues();
        cv.put(TableFeeds.COL_TITLE, feed.getTitle());
        cv.put(TableFeeds.COL_LINK, feed.getLink());
        cv.put(TableFeeds.COL_BUILTIN, 0);
        return database.insertWithOnConflict(TableFeeds.TABLE_NAME, null, cv, SQLiteDatabase.CONFLICT_REPLACE);

    }

    private static class DatabaseUpdateObservers extends Observable<DatabaseUpdateObserver>{
        public void notifyObserversDatabaseChanged(){
            for(DatabaseUpdateObserver observer : mObservers){
                observer.onDatabaseUpdate();
            }
        }
    }

    public void regiserObserver(DatabaseUpdateObserver observer){
        observers.registerObserver(observer);
    }

    public void unregisterObserver(DatabaseUpdateObserver observer){
        observers.unregisterObserver(observer);
    }

    public void unregisterAllObservers(){
        observers.unregisterAll();
    }

    public void addBuiltInFeeds(SQLiteDatabase db){
        ContentValues cv1 = new ContentValues();
        cv1.put(TableFeeds.COL_TITLE, "Разработка под Android");
        cv1.put(TableFeeds.COL_LINK, "https://habrahabr.ru/rss/hub/android_dev/");
        cv1.put(TableFeeds.COL_BUILTIN, 1);

        ContentValues cv2 = new ContentValues();
        cv2.put(TableFeeds.COL_TITLE, "Android Developers Blog");
        cv2.put(TableFeeds.COL_LINK, "http://feeds.feedburner.com/blogspot/hsDu");
        cv2.put(TableFeeds.COL_BUILTIN, 1);

        ContentValues cv3 = new ContentValues();
        cv3.put(TableFeeds.COL_TITLE, "JavaWorld");
        cv3.put(TableFeeds.COL_LINK, "http://www.javaworld.com/index.rss");
        cv3.put(TableFeeds.COL_BUILTIN, 1);

        db.insert(TableFeeds.TABLE_NAME, null, cv1);
        db.insert(TableFeeds.TABLE_NAME, null, cv2);
        db.insert(TableFeeds.TABLE_NAME, null, cv3);
    }
}
