package ru.sidm.rssreader.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import ru.sidm.rssreader.R;

/**
 * Created by sidm on 01.03.2016.
 */
public class AddFeedDialog extends DialogFragment {

    private String titleText;
    private String link;
    private AddFeedDialogListener listener;

    public interface AddFeedDialogListener{
        void onFeedLinkEntered(String feedLink);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            this.listener = (AddFeedDialogListener)getActivity();
        }catch(Exception e){
            throw new UnsupportedOperationException("Activity must implement AddFeedDialogListener");
        }
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }

    public void setLink(String link){
        this.link = link;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(savedInstanceState != null){
            titleText = savedInstanceState.getString("title");
            link = savedInstanceState.getString("link");
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater  inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.dialog_add_feed, null);
        final TextView tvLink = (TextView)view.findViewById(R.id.feed_link);
        tvLink.setText(link);

        builder.setTitle(titleText)
                .setView(view)
                .setPositiveButton(getResources().getString(R.string.add_feed_button), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        listener.onFeedLinkEntered(tvLink.getText().toString());
                    }
                })
                .setNegativeButton(getResources().getString(R.string.cancel), null);

        return builder.create();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("title", titleText);
        outState.putString("link", link);
    }

}
