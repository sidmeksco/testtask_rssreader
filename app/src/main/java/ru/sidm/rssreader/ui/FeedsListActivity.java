package ru.sidm.rssreader.ui;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;


import ru.sidm.rssreader.R;
import ru.sidm.rssreader.objects.Feed;
import ru.sidm.rssreader.services.FeedsUpdaterService;
import ru.sidm.rssreader.ui.adapters.FeedsListAdapter;
import ru.sidm.rssreader.ui.loaders.FeedsListLoader;
import ru.sidm.rssreader.ui.loaders.NewFeedCheckerLoader;

public class FeedsListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, AddFeedDialog.AddFeedDialogListener{

    private ListView lvList;
    private String feedLink;
    private static final int LOADER_ID = 1;
    private static final int LOADER_ADD_FEED_ID = 2;
    private static final String DIALOG_TITLE_KEY = "ru.sidm.rssreader.dialogtitle";
    private static final String DIALOG_LINK_KEY = "ru.sidm.rssreader.link";
    private static final int MSG_SHOW_DIALOG = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feeds_list);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lvList = (ListView)findViewById(R.id.list);

        setTitle(getResources().getString(R.string.feed_activity_title));
        lvList.setOnItemClickListener(this);
        getLoaderManager().initLoader(LOADER_ID, null, loadFeedsLoaderListener);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.feeds_activity_menu, menu);
        return true;
    }

    public boolean onMenuItemClick(MenuItem item){
        switch(item.getItemId()){
            case R.id.menu_add:
                AddFeedDialog dialog = new AddFeedDialog();
                dialog.setTitleText(getResources().getString(R.string.add_feed_link_title));
                dialog.show(getFragmentManager(), "dialog");
                break;
            case R.id.menu_update:
                Intent intent = new Intent(this, FeedsUpdaterService.class);
                startService(intent);
                break;
            default:
        }

        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //Toast.makeText(this,"selected  " + position, Toast.LENGTH_LONG).show();

        Intent intent = new Intent(this, ItemsListActivity.class);
        Feed feed = ((Feed)lvList.getAdapter().getItem(position));
        intent.putExtra(Feed.FEED_ID, feed.getId());
        intent.putExtra(Feed.FEED_NAME, feed.getTitle());
        startActivity(intent);
    }



    @Override
    public void onFeedLinkEntered(String feedLink) {
        if(feedLink.isEmpty()){
            showNewFeedLinkDialog(getResources().getString(R.string.add_feed_link_title), "");
            return;
        }
        this.feedLink = feedLink;
        Bundle bundle = new Bundle();
        bundle.putString(DIALOG_LINK_KEY, feedLink);
        getLoaderManager().restartLoader(LOADER_ADD_FEED_ID, bundle, addNewFeedLoaderListener);
    }

    private void showNewFeedLinkDialog(String titleText, String link){
        AddFeedDialog dialog = new AddFeedDialog();
        dialog.setTitleText(titleText);
        dialog.setLink(link);
        dialog.show(getFragmentManager(), "dialog");
    }

    private LoaderManager.LoaderCallbacks<Feed[]> loadFeedsLoaderListener = new LoaderManager.LoaderCallbacks<Feed[]>(){
        @Override
        public Loader<Feed[]> onCreateLoader(int id, Bundle args) {
            return new FeedsListLoader(getApplicationContext());
        }

        @Override
        public void onLoadFinished(Loader<Feed[]> loader, Feed[] data) {
            lvList.setAdapter(new FeedsListAdapter(getApplicationContext(), R.layout.feed_item, data));
        }

        @Override
        public void onLoaderReset(Loader<Feed[]> loader) {
            lvList.setAdapter(null);
        }
    };

    private LoaderManager.LoaderCallbacks<Boolean> addNewFeedLoaderListener = new LoaderManager.LoaderCallbacks<Boolean>(){
        @Override
        public Loader<Boolean> onCreateLoader(int id, Bundle args) {
            return new NewFeedCheckerLoader(getApplicationContext(), args.getString(DIALOG_LINK_KEY));
        }

        @Override
        public void onLoadFinished(Loader<Boolean> loader, Boolean data) {
            if(data == true)
                getLoaderManager().restartLoader(LOADER_ID, null, loadFeedsLoaderListener);
            else{
                //showNewFeedLinkDialog("Incorrect link. Enter correct RSS feed link");
                Message msg = new Message();
                msg.what = MSG_SHOW_DIALOG;
                Bundle bundle = new Bundle();
                bundle.putString(DIALOG_TITLE_KEY, "Incorrect link. Enter correct RSS feed link");
                bundle.putString(DIALOG_LINK_KEY, feedLink);
                msg.setData(bundle);
                handler.sendMessage(msg);
            }

        }

        @Override
        public void onLoaderReset(Loader<Boolean> loader) {

        }
    };

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(msg.what == MSG_SHOW_DIALOG) {
               showNewFeedLinkDialog(msg.getData().getString(DIALOG_TITLE_KEY), msg.getData().getString(DIALOG_LINK_KEY));
            }
        }
    };
}
