package ru.sidm.rssreader.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import ru.sidm.rssreader.R;

import ru.sidm.rssreader.objects.Item;

/**
 * Created by sidm on 27.02.2016.
 */
public class ItemsListAdapter extends ArrayAdapter<Item>{

    private LayoutInflater layoutInflater;
    private Context context;
    private int resource;

    public ItemsListAdapter(Context context, int resource, Item[] objects) {
        super(context, resource, objects);

        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = layoutInflater.inflate(resource, null);
            ViewHolder vh = new ViewHolder();
            vh.title = (TextView)convertView.findViewById(R.id.item_title);
            vh.date = (TextView)convertView.findViewById(R.id.item_date);
            convertView.setTag(vh);
        }

        ViewHolder vh = (ViewHolder)convertView.getTag();

        vh.title.setText(getItem(position).getTitle());
        vh.date.setText(getItem(position).getDate().toString());

        return convertView;
    }

    static class ViewHolder{
        public TextView title;
        public TextView date;
    }
}
