package ru.sidm.rssreader.ui.loaders;

import android.app.LoaderManager;
import android.content.AsyncTaskLoader;
import android.content.Context;

import java.util.List;

import ru.sidm.rssreader.db.DatabaseHelper;
import ru.sidm.rssreader.objects.Item;

/**
 * Created by sidm on 28.02.2016.
 */
public class ItemsListLoader extends AsyncTaskLoader<Item[]>{

    private Context context;
    private int feedId;

    public ItemsListLoader(Context context, int feedId){
        super(context);

        this.context = context;
        this.feedId = feedId;
    }

    @Override
    public Item[] loadInBackground() {
        DatabaseHelper dbh = DatabaseHelper.getInstance(context);
        List<Item> items = dbh.getItemsByFeedID(feedId);

        Item[] itemsArray = items.toArray(new Item[items.size()]);
        return itemsArray;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }
}
