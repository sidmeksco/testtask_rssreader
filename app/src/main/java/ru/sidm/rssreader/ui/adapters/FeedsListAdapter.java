package ru.sidm.rssreader.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import ru.sidm.rssreader.R;

import ru.sidm.rssreader.objects.Feed;

/**
 * Created by sidm on 26.02.2016.
 */
public class FeedsListAdapter extends ArrayAdapter<Feed> {

    private LayoutInflater inflater;
    private Context context;
    private int resource;

    public FeedsListAdapter(Context context, int resource, Feed[] objects) {
        super(context, resource, objects);

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.resource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //return super.getView(position, convertView, parent);
        if(convertView == null){
            convertView = inflater.inflate(resource, null);
            ViewHolder vh = new ViewHolder();
            vh.tvFeedTitle = (TextView)convertView.findViewById(R.id.feed_title);
            convertView.setTag(vh);
        }

        ViewHolder vh = (ViewHolder)convertView.getTag();
        vh.tvFeedTitle.setText(getItem(position).getTitle());

        return convertView;
    }


    static class ViewHolder{
        public TextView tvFeedTitle;
    }

}
