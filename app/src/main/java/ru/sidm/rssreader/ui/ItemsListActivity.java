package ru.sidm.rssreader.ui;


import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import ru.sidm.rssreader.R;
import ru.sidm.rssreader.db.DatabaseHelper;
import ru.sidm.rssreader.objects.Feed;
import ru.sidm.rssreader.objects.Item;
import ru.sidm.rssreader.ui.adapters.ItemsListAdapter;
import ru.sidm.rssreader.ui.loaders.ItemsListLoader;

public class ItemsListActivity extends AppCompatActivity implements DatabaseHelper.DatabaseUpdateObserver, AdapterView.OnItemClickListener, LoaderManager.LoaderCallbacks<Item[]> {

    private ListView lvItemsList;
    private TextView tvTitle;
    private int feedID;
    private static final int LOADER_ID = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items_list);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        lvItemsList = (ListView)findViewById(R.id.list);
        tvTitle = (TextView)findViewById(R.id.feed_title);

        Intent intent = getIntent();
        feedID = intent.getIntExtra(Feed.FEED_ID, -1);
        String title = intent.getStringExtra(Feed.FEED_NAME);

        tvTitle.setText(title);
        lvItemsList.setOnItemClickListener(this);

        setTitle(title);

        getLoaderManager().initLoader(LOADER_ID, null, this);
    }


    @Override
    protected void onStop() {
        super.onStop();
        DatabaseHelper.getInstance(this).unregisterObserver(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        DatabaseHelper.getInstance(this).regiserObserver(this);
    }

    @Override
    public void onDatabaseUpdate() {
        getLoaderManager().restartLoader(LOADER_ID, null, this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Intent intent = new Intent(this, ItemDetailActivity.class);
        Item item = (Item)lvItemsList.getAdapter().getItem(position);
        intent.putExtra(Item.ITEM_ID, item.getItem_id());
        startActivity(intent);
    }

    @Override
    public Loader<Item[]> onCreateLoader(int id, Bundle args) {
        return new ItemsListLoader(this, feedID);
    }

    @Override
    public void onLoadFinished(Loader<Item[]> loader, Item[] data) {
        ItemsListAdapter adapter = new ItemsListAdapter(this, R.layout.item_item, data);
        lvItemsList.setAdapter(adapter);
    }

    @Override
    public void onLoaderReset(Loader<Item[]> loader) {
        lvItemsList.setAdapter(null);
    }
}
