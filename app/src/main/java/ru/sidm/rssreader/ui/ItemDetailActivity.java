package ru.sidm.rssreader.ui;

import android.app.LoaderManager;
import android.content.Intent;
import android.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.TextView;

import ru.sidm.rssreader.R;
import ru.sidm.rssreader.objects.Item;
import ru.sidm.rssreader.ui.loaders.ItemDetailLoader;

public class ItemDetailActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Item>{

    private static final int LOADER_ID = 3;
    private String itemId;
    private Item item;
    private TextView tvTitle;
    private WebView wvDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();

        itemId = intent.getStringExtra(Item.ITEM_ID);

        tvTitle = (TextView)findViewById(R.id.item_title);
        wvDescription = (WebView)findViewById(R.id.item_description);

        getLoaderManager().initLoader(LOADER_ID, null, this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader<Item> onCreateLoader(int id, Bundle args) {
        return new ItemDetailLoader(this, itemId);
    }

    @Override
    public void onLoadFinished(Loader<Item> loader, Item data) {
        this.item = data;
        fillData();
    }

    @Override
    public void onLoaderReset(Loader<Item> loader) {

    }

    public void fillData(){
        tvTitle.setText(item.getTitle());
        wvDescription.loadDataWithBaseURL(null, item.getDescription(), null, null, null);
        setTitle(item.getTitle());
    }
}
