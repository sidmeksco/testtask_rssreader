package ru.sidm.rssreader.ui.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

import ru.sidm.rssreader.db.DatabaseHelper;
import ru.sidm.rssreader.objects.Item;

/**
 * Created by sidm on 29.02.2016.
 */
public class ItemDetailLoader extends AsyncTaskLoader<Item>{

    private Context context;
    private String itemId;

    public ItemDetailLoader(Context context, String itemId) {
        super(context);
        this.context = context;
        this.itemId = itemId;
    }

    @Override
    public Item loadInBackground() {
        DatabaseHelper dbh = DatabaseHelper.getInstance(context);
        Item item = dbh.getItemByID(itemId);

        return item;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }
}
