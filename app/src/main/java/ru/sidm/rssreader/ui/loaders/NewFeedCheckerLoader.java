package ru.sidm.rssreader.ui.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import ru.sidm.rssreader.db.DatabaseHelper;
import ru.sidm.rssreader.engine.FeedLoader;
import ru.sidm.rssreader.objects.Feed;
import ru.sidm.rssreader.objects.Item;

/**
 * Created by sidm on 01.03.2016.
 */
public class NewFeedCheckerLoader extends AsyncTaskLoader<Boolean>{

    private Context context;
    private String link;

    public NewFeedCheckerLoader(Context context, String link){
        super(context);
        this.context = context;
        this.link = link;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public Boolean loadInBackground() {

        ru.sidm.rssreader.earlrsslib.Feed earlFeed = FeedLoader.loadFeedItems(link);
        if(earlFeed == null || earlFeed.getItems().size()==0){
            return false;
        }

        Feed feed = new Feed(earlFeed.getTitle(), earlFeed.getLink());

        DatabaseHelper dbh = DatabaseHelper.getInstance(context);
        long id = dbh.addFeedToDB(feed);
        feed.setId((int) id);

        List<Item> items = new ArrayList<>(earlFeed.getItems().size());
        for (ru.sidm.rssreader.earlrsslib.Item earlItem : earlFeed.getItems()) {
            items.add(new Item(feed, earlItem));
        }
        dbh.addItemsToDB(items);

        return true;
    }


}
