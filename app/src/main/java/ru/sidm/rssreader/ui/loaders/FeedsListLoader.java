package ru.sidm.rssreader.ui.loaders;

import android.content.AsyncTaskLoader;
import android.content.Context;

import java.util.List;

import ru.sidm.rssreader.db.DatabaseHelper;
import ru.sidm.rssreader.objects.Feed;

/**
 * Created by sidm on 26.02.2016.
 */
public class FeedsListLoader extends AsyncTaskLoader<Feed[]>{

    private Context context;

    public FeedsListLoader(Context context) {
        super(context);

        this.context = context;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        forceLoad();
    }

    @Override
    public Feed[] loadInBackground() {
        DatabaseHelper dbh = DatabaseHelper.getInstance(context);
        List<Feed> feedList = dbh.getFeeds();

        Feed[] feedArray = feedList.toArray(new Feed[feedList.size()]);
        return feedArray;
    }
}
