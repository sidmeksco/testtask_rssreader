package ru.sidm.rssreader.objects;

import android.database.Cursor;

import java.text.DateFormat;
import java.util.Date;

import ru.sidm.rssreader.db.TableFeeds;

/**
 * Created by sidm on 26.02.2016.
 */
public class Feed {

    private int id;
    private String title;
    private String link;
    private Date lastupdate;
    private boolean builtin;

    public static final String FEED_ID = "ru.sidm.rssreader.feed_id";
    public static final String FEED_NAME = "ru.sidm.rssreader.feed_name";

    public Feed(String title, String link){
        this.title = title;
        this.link = link;
        this.builtin = false;
    }

    public Feed(int id, String title, String link, Date lastupdate, boolean builtin) {
        this.id = id;
        this.title = title;
        this.link = link;
        this.lastupdate = lastupdate;
        this.builtin = builtin;
    }

    public Feed(Cursor cursor){
        this.id = cursor.getInt(cursor.getColumnIndex(TableFeeds._ID));
        this.title = cursor.getString(cursor.getColumnIndex(TableFeeds.COL_TITLE));
        this.link = cursor.getString(cursor.getColumnIndex(TableFeeds.COL_LINK));
        this.lastupdate = new Date(cursor.getLong(cursor.getColumnIndex(TableFeeds.COL_LAST_UPDATE)));
        this.builtin = 1 == cursor.getInt(cursor.getColumnIndex(TableFeeds.COL_BUILTIN));
    }

    public void setId(int id){
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getLink() {
        return link;
    }

    public Date getLastupdate() {
        return lastupdate;
    }

    public boolean isBuiltin() {
        return builtin;
    }
}
