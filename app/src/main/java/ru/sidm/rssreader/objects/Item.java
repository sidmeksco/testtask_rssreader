package ru.sidm.rssreader.objects;

import android.database.Cursor;

import java.util.Date;

import ru.sidm.rssreader.db.TableItems;

/**
 * Created by sidm on 26.02.2016.
 */
public class Item {

    private String item_id; //id from internet
    private int feed_id;
    private String title;
    private Date date;
    private String link;
    private String description;

    public static final String ITEM_ID = "ru.sidm.rssreader.item_id";

    public Item(Cursor cursor){
        this.item_id = cursor.getString(cursor.getColumnIndex(TableItems.COL_ITEM_ID));
        this.feed_id = cursor.getInt(cursor.getColumnIndex(TableItems.COL_FEED_ID));
        this.date = new Date(cursor.getLong(cursor.getColumnIndex(TableItems.COL_DATE)));
        this.title = cursor.getString(cursor.getColumnIndex(TableItems.COL_TITLE));
        this.description = cursor.getString(cursor.getColumnIndex(TableItems.COL_DESCRIPTION));
        this.link = cursor.getString(cursor.getColumnIndex(TableItems.COL_LINK));
    }

    public Item(Feed feed, ru.sidm.rssreader.earlrsslib.Item item){
        String id = item.getId();
        if(id==null || id.isEmpty())
            id = item.getLink();
        this.item_id = id;
        this.feed_id = feed.getId();
        this.date = item.getPublicationDate();
        this.title = item.getTitle();
        this.description = item.getDescription();
        this.link = item.getLink();
    }

    public String getItem_id() {
        return item_id;
    }

    public int getFeed_id() {
        return feed_id;
    }

    public String getTitle() {
        return title;
    }

    public Date getDate() {
        return date;
    }

    public String getLink() {
        return link;
    }

    public String getDescription() {
        return description;
    }

}
