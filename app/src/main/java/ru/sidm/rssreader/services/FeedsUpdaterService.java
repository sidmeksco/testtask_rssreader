package ru.sidm.rssreader.services;

import android.app.IntentService;
import android.content.Intent;


import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import ru.sidm.rssreader.db.DatabaseHelper;
import ru.sidm.rssreader.earlrsslib.EarlParser;
import ru.sidm.rssreader.engine.FeedLoader;
import ru.sidm.rssreader.objects.Feed;
import ru.sidm.rssreader.objects.Item;


public class FeedsUpdaterService extends IntentService {

    public FeedsUpdaterService() {
        super("FeedsUpdaterService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        DatabaseHelper dbh = DatabaseHelper.getInstance(this);
        List<Feed> feeds = dbh.getFeeds();

        for(Feed feed : feeds){
            updateFeed(feed, dbh);
        }
    }

    private void updateFeed(Feed feed, DatabaseHelper dbh){

        ru.sidm.rssreader.earlrsslib.Feed earlFeed = FeedLoader.loadFeedItems(feed.getLink());
        if(earlFeed == null)
            return;

        List<Item> items = new ArrayList<>(earlFeed.getItems().size());
        for (ru.sidm.rssreader.earlrsslib.Item earlItem : earlFeed.getItems()) {
            items.add(new Item(feed, earlItem));
        }
        dbh.addItemsToDB(items);
    }

}
