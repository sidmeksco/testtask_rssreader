package ru.sidm.rssreader.engine;


import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import ru.sidm.rssreader.earlrsslib.EarlParser;
import ru.sidm.rssreader.objects.Item;

/**
 * Created by sidm on 01.03.2016.
 */
public class FeedLoader {

    public static ru.sidm.rssreader.earlrsslib.Feed loadFeedItems(String urlLink){

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        ru.sidm.rssreader.earlrsslib.Feed earlFeed = null;

        try {
            URL url = new URL(checkAndCorrectURL(urlLink));
            urlConnection = (HttpURLConnection)url.openConnection();
            if(urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK){
                inputStream = urlConnection.getInputStream();
                earlFeed = EarlParser.parseOrThrow(inputStream, 0);
            }
        } catch(Exception e){
            e.printStackTrace();

        }finally{
            if(urlConnection != null)
                urlConnection.disconnect();
        }

        return earlFeed;
        /*
        InputStream inputStream = new URL(link).openConnection().getInputStream();

        Feed feed = EarlParser.parseOrThrow(inputStream, 0);
        Log.i(TAG, "Processing feed: " + feed.getTitle());
        for (Item item : feed.getItems()) {
            String title = item.getTitle();
            Log.i(TAG, "Item title: " + (title == null ? "N/A" : title));
        }
        */


    }

    private static String checkAndCorrectURL(String urlLink){
        if(!urlLink.startsWith("http://") && !urlLink.startsWith("https://"))
            return "http://" + urlLink;
        return urlLink;
    }
}
